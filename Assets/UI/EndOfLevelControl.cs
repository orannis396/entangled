﻿using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Entangled.UI{
    internal class EndOfLevelControl : MonoBehaviour{
        public GameObject UIPanel = null;
        public GameObject[] Teacups = {null, null, null};
        public Text LevelTitle = null;
        public Text TimerText = null;
        public Text ThreadUsage = null;
        public Button DefaultButton = null;
        private Statistics stats = null;
        private int ticksSinceAwake = 0;
        public static bool IsCurrentlyActive = false;

        private void Start(){
            IsCurrentlyActive = true;
        }

        public void Awake(){
            if (!MenuScript.ControllerConnected) {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            if (MenuScript.ControllerConnected) {
                GameObject myEventSystem = GameObject.Find("EventSystem");
                if (myEventSystem != null) {
                    myEventSystem.GetComponent<EventSystem>().SetSelectedGameObject(null);
                }
                DefaultButton.Select();
            }
            stats = this.GetComponent<Statistics>();
            stats.Calculate();
            LevelTitle.text = Regex.Replace(CylinderPlayer.LevelDictionary[stats.Player.CurrentLevel],
                @"(\B[A-Z]+?(?=[A-Z][^A-Z])|\B[A-Z]+?(?=[^A-Z]))", " $1");
            for (int i = 0; i < stats.TeacupCount; i++) {
                Teacups[i].SetActive(true);
            }
            ThreadUsage.text = ThreadUsage.text.Replace("%s", stats.ThreadUsed);
            TimerText.text = stats.TimeTaken;
        }

        public void FixedUpdate(){
            ticksSinceAwake++;
        }

        public void ContinueButton(){
            string nextLevel = CylinderPlayer.LevelDictionary.ContainsKey(stats.Player.CurrentLevel + 1)
                ? CylinderPlayer.LevelDictionary[stats.Player.CurrentLevel + 1]
                : "MainMenu";
            if (nextLevel == "MainMenu") {
                MusicControl.PlayMenuMusic();
            }
            DeferredDestroy.TriggerDestroy();
            SceneManager.LoadScene(nextLevel);
        }

        public void Update(){}
    }
}