﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Entangled.UI{
    public class MenuScript : MonoBehaviour{
        public GameObject MainPanel = null;
        public GameObject LevelPanel = null;
        public Button MainDefaultButton = null;
        public Button LevelDefaultButton = null;
        private static bool controllerConnected = false;
        private static bool controllerEvaluated = false;

        public static bool ControllerConnected{
            get{
                if (!controllerEvaluated) {
                    EvalController();
                }
                return controllerConnected;
            }
            set { controllerConnected = value; }
        }

        public void LoadScene(string scene){
            SceneManager.LoadScene(scene);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            MusicControl.PlayGameMusic();
        }

        public void LevelSelectActivate(){
            if (controllerConnected) {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
            }
            if (LevelPanel != null) {
                MainPanel.SetActive(false);
                LevelPanel.SetActive(true);
                if (controllerConnected) {
                    GameObject myEventSystem = GameObject.Find("EventSystem");
                    if (myEventSystem != null) {
                        myEventSystem.GetComponent<EventSystem>().SetSelectedGameObject(null);
                    }
                    LevelDefaultButton.Select();
                }
            }
        }

        public void MainActivate(){
            if (controllerConnected) {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
            }
            if (MainPanel != null) {
                MainPanel.SetActive(true);
                LevelPanel.SetActive(false);
                if (controllerConnected) {
                    GameObject myEventSystem = GameObject.Find("EventSystem");
                    if (myEventSystem != null) {
                        myEventSystem.GetComponent<EventSystem>().SetSelectedGameObject(null);
                    }
                    MainDefaultButton.Select();
                }
            }
        }

        public void ExitGame(){
            Application.Quit();
        }

        public void Awake(){
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            MainPanel.SetActive(false);
            LevelPanel.SetActive(false);
            EvalController();
            this.MainActivate();
        }

        private static void EvalController(){
            int joycount = 0;

            foreach (string joystickName in Input.GetJoystickNames()) {
                if (!string.IsNullOrEmpty(joystickName)) {
                    Debug.Log("Joystick connected: " + joystickName);
                    joycount++;
                }
            }
            if (joycount > 0) {
                controllerConnected = true;
            }
            controllerEvaluated = true;
        }

        public void Update(){
            if (Input.GetKeyUp(KeyCode.Escape)) {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
        }
    }
}