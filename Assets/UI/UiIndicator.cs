﻿using UnityEngine;

namespace Entangled.UI{
    class UiIndicator : MonoBehaviour{
        public GameObject[] TeacupUiHidden = {null, null, null};
        public GameObject[] TeacupUiDisplayed = {null, null, null};
        private bool[] movin = null;
        private int currentActiveTeacupIndex = -1;
        private float movementT = 0.08f;
        public Vector3 velocity = new Vector3(0.1f, 0.1f, 0.1f);

        private static UiIndicator instance;

        public static int CurrentTeacupCount(){
            return instance.currentActiveTeacupIndex + 1;
        }

        public void Awake(){
            movin = new bool[3];
            for (int i = 0; i < 3; i++) {
                if (TeacupUiHidden[i] == null || TeacupUiDisplayed[i] == null) {
                    Debug.LogError("No teacup set!");
                }
                movin[i] = false;
            }
            instance = this;
        }

        public void FixedUpdate(){
            for (int i = 0; i < movin.Length; i++) {
                bool v = movin[i];
                if (v) {
                    RectTransform thisVector = TeacupUiDisplayed[i].GetComponent<RectTransform>();
                    RectTransform targetVector = TeacupUiHidden[i].GetComponent<RectTransform>();
                    thisVector.sizeDelta =
                        new Vector2(Mathf.Lerp(thisVector.sizeDelta.x, targetVector.sizeDelta.x, movementT),
                            Mathf.Lerp(thisVector.sizeDelta.y, targetVector.sizeDelta.y, movementT));
                    thisVector.anchoredPosition =
                        new Vector2(
                            Mathf.Lerp(thisVector.anchoredPosition.x, targetVector.anchoredPosition.x, movementT),
                            Mathf.Lerp(thisVector.anchoredPosition.y, targetVector.anchoredPosition.y, movementT));
                }
            }
        }

        public static void IncrementTeacup(Vector3 triggerPoint){
            if (instance != null) {
                instance.IncrementTeacupLocal(triggerPoint);
            } else {
                Debug.LogError("Teacup ui element not found");
            }
        }

        private void IncrementTeacupLocal(Vector3 triggerPoint){
            if (currentActiveTeacupIndex >= 2) {
                return;
            }
            Camera cam = Camera.main;
            ++currentActiveTeacupIndex;
            Vector3 startPos = cam.WorldToScreenPoint(triggerPoint);
            TeacupUiDisplayed[currentActiveTeacupIndex].GetComponent<RectTransform>().anchoredPosition =
                new Vector3((startPos.x - 0), -(startPos.y - 0), startPos.z);
            TeacupUiDisplayed[currentActiveTeacupIndex].SetActive(true);
            movin[currentActiveTeacupIndex] = true;
        }
    }
}