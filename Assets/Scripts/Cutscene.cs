﻿using UnityEngine;
using UnityEngine.UI;

namespace Entangled{
    public class Cutscene : MonoBehaviour{
        private Animation anim;
        private bool checkForAnimFinish = true;

        private AudioListener cutsceneAudioListener;

        public UnityEngine.Camera CutsceneCam;
        public GameObject Player;
        public UnityEngine.Camera PostCutsceneCam;
        private AudioListener postCutsceneAudioListener;

        // Use this for initialization

        private void Start(){
            cutsceneAudioListener = CutsceneCam.GetComponent<AudioListener>();
            postCutsceneAudioListener = PostCutsceneCam.GetComponent<AudioListener>();

            CutsceneCam.enabled = true;
            PostCutsceneCam.enabled = false;

            cutsceneAudioListener.enabled = true;
            postCutsceneAudioListener.enabled = false;

            anim = CutsceneCam.GetComponent<Animation>();

            Player.GetComponent<CylinderPlayer>().InCutscene = true;
        }
        
        private void Update(){
            if (Player.GetComponent<CylinderPlayer>().InCutscene)
            {
                if (Input.anyKey) {
                    this.EndCutscene();
                }
                if (checkForAnimFinish) {
                    if (!anim.isPlaying || !anim.enabled) {
                        this.EndCutscene();
            }
                
                }
                if (checkForAnimFinish) {
                    if (!anim.isPlaying || !anim.enabled) {
                        EndCutscene();
                    }
                }
            }
        }

        private void EndCutscene(){
            CylinderPlayer p = Player.GetComponent<CylinderPlayer>();

            anim.enabled = false;
            p.CanMove = true;
            p.InCutscene = false;

            CutsceneCam.enabled = false;
            PostCutsceneCam.enabled = true;

            cutsceneAudioListener.enabled = false;
            postCutsceneAudioListener.enabled = true;

            checkForAnimFinish = false;
            p.InCutscene = false;
        }
    }
}