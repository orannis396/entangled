﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Debug = UnityEngine.Debug;

namespace Entangled.UI{
    /// <summary>
    /// Script for menu that appears when the game has been paused.
    /// </summary>
    public class PauseMenu : MonoBehaviour{
        private Stopwatch Timer = null; // times the player's progress in the level
        private CylinderPlayer Player = null; // the player
        public GameObject Menu = null; // the ui objects of the menu
        public Button DefaultButton = null; // the button selected by default
        private bool Paused = false; // true if the game is currently paused

        // Use this for initialization
        void Start(){
            FindPlayer();
            Timer = Player.timer;
        }

        /// <summary>
        /// Searches for the player gameobject and updates the player field
        /// </summary>
        private void FindPlayer(){
            GameObject[] playerObj = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject o in playerObj) {
                CylinderPlayer cylPl = o.GetComponent<CylinderPlayer>();
                if (cylPl != null) {
                    Player = cylPl;
                    break;
                }
            }
        }

        void Update(){
            // check for user input
            if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.JoystickButton7)) {
                if (Paused) {
                    Unpause();
                } else {
                    Pause();
                }
            }
        }

        /// <summary>
        /// Pause the game and bring up the menu
        /// </summary>
        public void Pause(){
            // if we're not in game, don't pause
            if (EndOfLevelControl.IsCurrentlyActive) {
                return;
            }

            // pause the game
            Paused = true;
            Cursor.visible = true;
            Player.CanMove = false;
            Timer.Stop();
            // bring up menu
            Menu.SetActive(true);

            // if controller is connected, select default button
            if (MenuScript.ControllerConnected) {
                GameObject myEventSystem = GameObject.Find("EventSystem");
                if (myEventSystem != null) {
                    myEventSystem.GetComponent<EventSystem>().SetSelectedGameObject(null);
                }
                DefaultButton.Select();
            }
        }

        /// <summary>
        /// Unpause the game and hide the menu
        /// </summary>
        public void Unpause(){
            Paused = false;
            Cursor.visible = false;
            Menu.SetActive(false);
            Timer.Start();
            Player.CanMove = true;
        }

        /// <summary>
        /// Return to main menu
        /// </summary>
        public void MainMenu(){
            SceneManager.LoadScene("MainMenu");
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            MusicControl.PlayMenuMusic();
        }
    }
}