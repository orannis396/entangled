﻿using UnityEngine;
using UnityEngine.UI;

namespace Entangled{
    public class TutTextTrigger : MonoBehaviour{
        public Text TutText;

        private bool startChange = false;

        // Use this for initialization
        void Start(){}

        // Update is called once per frame
        void Update(){
            if (!startChange) return;
            if (startChange) {
                Color col = TutText.color;
                col.a -= 0.01f;
                TutText.color = col;
            }
            if (TutText.color.a <= 0f) startChange = false;
        }

        void OnTriggerEnter(Collider c){
            if (c.gameObject.CompareTag("Player")) {
                startChange = true;
            }
        }
    }
}