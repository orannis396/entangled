﻿using UnityEngine;
using UnityEngine.UI;

namespace Entangled {
    class CylinderCamera : MonoBehaviour {
        public GameObject Player = null;
        public Image OverlayImage = null;

        private CylinderPlayer playerScript;
        private float localRotOffset;

        public float CurrentDistanceToPlayer;
        public float CurrentCameraHeight;

        public float WallToCameraGap = 0.5f;
        public float DistanceToPlayer = 6f;

        private float rotateMultiplier = 2f;

        private float roomToMove = 1f;

        public void Start() {
            playerScript = Player.GetComponent<CylinderPlayer>();
            localRotOffset = 0f;
            CurrentDistanceToPlayer = DistanceToPlayer;
            CurrentCameraHeight = transform.position.y;
        }

        private void FadeCameraIn() {
            if (OverlayImage.color.a > 0f) {
                Color c = OverlayImage.color;
                c.a -= 0.01f;
                OverlayImage.color = c;
            }
        }

        public void FixedUpdate() {
            FadeCameraIn();
            if (Input.GetKeyDown(KeyCode.RightControl) && Cursor.visible) {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.None;
            }
            if (Input.GetKeyDown(KeyCode.LeftControl) && !Cursor.visible) {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }

            if (playerScript.InCutscene || !playerScript.CanMove) return;


            /*
            * Ray casting for vision obstruction of player
            * distanceToPlayer is updated to be the new max distance
            */
            CurrentDistanceToPlayer = Vector3.Distance(Player.transform.position, transform.position);
            RaycastHit objectHit;
            Debug.DrawRay(Player.transform.position, transform.forward * CurrentDistanceToPlayer * -1, Color.red);
            Debug.DrawRay(Player.transform.position - (transform.forward * (int)DistanceToPlayer), transform.forward * DistanceToPlayer, Color.green);

            if (Physics.Raycast(Player.transform.position, transform.forward * -1, out objectHit, (int)DistanceToPlayer)) {//raycast from player to camera
                GameObject obj = objectHit.collider.gameObject;
                if (!obj.CompareTag("Player") && !obj.CompareTag("Thread Pickup") && !obj.CompareTag("Trigger Box") && !obj.CompareTag("Thread") && !obj.CompareTag("Canvas") && !obj.CompareTag("MainCamera") && !obj.CompareTag("DoorRayTarget")) {
                    //Debug.Log("RAYCAST HIT PLAYER TO CAMERA at distance " + objectHit.distance);

                    float distanceToMoveForward = CurrentDistanceToPlayer - (objectHit.distance - WallToCameraGap);
                    transform.position += transform.forward * distanceToMoveForward;

                    //CurrentCameraHeight = 0.5f + (CurrentDistanceToPlayer * 0.45f);
                } else {//no hit from player to cam
                    RaycastHit camHit;
                    if (Physics.Raycast(Player.transform.position - (transform.forward * (int)DistanceToPlayer), transform.forward, out camHit, (int)DistanceToPlayer)) {//raycast from camera to player
                        //Debug.Log("HIT FROM CAM TO PLAYER " + camHit.collider.gameObject);
                    } else {//no hit from cam to player
                        if (CurrentDistanceToPlayer < DistanceToPlayer - roomToMove) {
                            //Debug.Log("No ray hit and too close");
                        } else if (CurrentDistanceToPlayer > DistanceToPlayer + roomToMove) {
                            //Debug.Log("No ray hit and too far");
                        } else {
                            //Debug.Log("No ray hit and within sweet spot");
                        }
                    }
                }
            }

            if (CurrentDistanceToPlayer > DistanceToPlayer + roomToMove) {
                transform.position += transform.forward * Mathf.Lerp(0f, CurrentDistanceToPlayer - DistanceToPlayer, 0.05f);
            }

            /*
            * Handle camera movement
            */
            float rightStickHoriz = Input.GetAxis("RightStickHoriz");
            transform.RotateAround(Player.transform.position, Vector3.up, rightStickHoriz * rotateMultiplier);
            localRotOffset += rightStickHoriz;

            float rightStickVert = Input.GetAxis("RightStickVert");
            CurrentCameraHeight = Mathf.Max(Mathf.Min(CurrentCameraHeight + rightStickVert, 5f), 0.4f);
            if (Input.GetMouseButton(1)) {
                float mousedx = Input.GetAxis("Mouse X");
                transform.RotateAround(Player.transform.position, Vector3.up, mousedx * rotateMultiplier);
                localRotOffset += mousedx;

                float mousedy = Input.GetAxis("Mouse Y") / 15f;
                CurrentCameraHeight = Mathf.Max(Mathf.Min(CurrentCameraHeight + mousedy * -1, 5f), 0.4f);
            }
            //transform.position = new Vector3(transform.position.x, Mathf.Max(Mathf.Min(transform.position.y + mousedy, 5f), 0.4f), transform.position.z);

            /*
            * Handle movement forwards from player input
            */
            //float yPos = transform.position.y;
            //Debug.Log(CurrentCameraHeight);
            float vel = playerScript.Velocity;
            transform.position = new Vector3(transform.position.x, Mathf.Lerp(transform.position.y, CurrentCameraHeight, 0.5f), transform.position.z);
            transform.LookAt(Player.transform);

            //transform.position += transform.forward * vel * 2.05f;
            if (vel > 0.01f) {
                float playerRotOffset = playerScript.RotOffset;

                float rotationDifference = localRotOffset - playerRotOffset;

                float rotateAmount = Mathf.Lerp(0f, rotationDifference, 0.05f);
                transform.RotateAround(Player.transform.position, Vector3.up, -1 * rotateAmount);
                this.localRotOffset += -1 * rotateAmount;
            }

        }
    }
}