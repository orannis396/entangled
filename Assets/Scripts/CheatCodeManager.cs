﻿using UnityEngine;
using System.Collections;
using Entangled;
using Entangled.UI;
using UnityEngine.SceneManagement;

namespace Entangled{
    /// <summary>
    /// Manages cheat code input and execution.
    /// </summary>
    public class CheatCodeManager : MonoBehaviour{
        private ThreadControl threadControl; // the player's thread control

        // which axes are in use
        private bool axisInUseInfinite = false;
        private bool axisInUseRed = false;
        private bool axisInUseBlue = false;
        private bool axisInUseYellow = false;
        private bool axisInUsePink = false;

        // Use this for initialization
        void Start(){
            threadControl = this.GetComponent<ThreadControl>();
        }

        // Update is called once per frame
        void Update(){
            // check for infinite thread
            if (Input.GetAxisRaw("InfiniteThread") != 0) {
                if (!axisInUseInfinite) {
                    threadControl.LimitedThread = !threadControl.LimitedThread;
                    axisInUseInfinite = true;
                }
            } else {
                axisInUseInfinite = false;
            }
            // check for red thread
            if (Input.GetAxisRaw("Red") != 0) {
                if (!axisInUseRed) {
                    threadControl.GiveColor(ThreadPickup.ColorType.Red);
                    axisInUseRed = true;
                }
            } else {
                axisInUseRed = false;
            }
            // check for blue thread
            if (Input.GetAxisRaw("Blue") != 0) {
                if (!axisInUseBlue) {
                    threadControl.GiveColor(ThreadPickup.ColorType.Blue);
                    axisInUseBlue = true;
                }
            } else {
                axisInUseBlue = false;
            }
            // check for yellow thread
            if (Input.GetAxisRaw("Yellow") != 0) {
                if (!axisInUseYellow) {
                    threadControl.GiveColor(ThreadPickup.ColorType.Yellow);
                    axisInUseYellow = true;
                }
            } else {
                axisInUseYellow = false;
            }
            // check for pink thread
            if (Input.GetAxisRaw("Pink") != 0) {
                if (!axisInUsePink) {
                    threadControl.GiveColor(ThreadPickup.ColorType.Pink);
                    axisInUsePink = true;
                }
            } else {
                axisInUsePink = false;
            }
            if (Input.GetAxisRaw("EndLevel") != 0) {
                if (EndOfLevelControl.IsCurrentlyActive) return;
                SceneManager.LoadScene("EndOfLevel", LoadSceneMode.Additive);
            }
        }
    }
}