﻿using Entangled.UI;
using UnityEngine;

namespace Entangled{
    [RequireComponent(typeof(AudioSource))]
    public class TeacupTrigger : MonoBehaviour{
        enum Dir{
            Up,
            Down
        };

        private AudioSource audioSource;
        public GameObject TeacupObject;
        public GameObject TeacupContainer;
        public GameObject TeacupPrefab;
        private GameObject player;

        private int ticksSincePickedUp = 0;
        private bool pickedUp = false;

        void Awake(){
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject pl in players) {
                ThreadControl tc = pl.GetComponent<ThreadControl>();
                if (tc != null) {
                    player = pl;
                    break;
                }
            }
        }

        void Start(){
            audioSource = this.GetComponent<AudioSource>();
        }

        void FixedUpdate(){
            if (pickedUp) {
                ticksSincePickedUp++;
            }

            if (pickedUp && !audioSource.isPlaying && ticksSincePickedUp > 10000) {
                TeacupContainer.gameObject.SetActive(false);
                Destroy(TeacupPrefab, 0.5f);
            }
        }

        void OnTriggerEnter(Collider c){
            if (!c.gameObject.CompareTag("Player")) return;
            pickedUp = true;
            if (audioSource.clip != null) {
                audioSource.Play();
            }
            TeacupObject.GetComponent<MeshRenderer>().enabled = false;
            ParticleSystem containerEmitter = TeacupContainer.GetComponentInChildren<ParticleSystem>(true);
            containerEmitter.Stop();
            Color col = containerEmitter.startColor;
            col.r = 8;
            col.g = 9;
            col.a = 0.1f;
            containerEmitter.startColor = col;
            containerEmitter.startLifetime = 3;
            containerEmitter.startSpeed = 8;
            containerEmitter.startSize = 0.17f;
            containerEmitter.gravityModifier = 0.008f;
            containerEmitter.Emit(5000);
            TeacupPrefab.GetComponentInChildren<BoxCollider>().enabled = false;
            UiIndicator.IncrementTeacup(TeacupObject.transform.position);
            player.GetComponent<CylinderPlayer>().TeacupScore++;
        }
    }
}