﻿using System;
using UnityEngine;
using System.Collections;
using Entangled.UI;

namespace Entangled{
    /// <summary>
    /// A class that brings together the statistics for the end-level screen.
    /// </summary>
    public class Statistics : MonoBehaviour{
        public CylinderPlayer Player = null; // The player
        public int TeacupCount = -1; // the number of teacups collected
        public string ThreadUsed = null; // the amount of thread used (metres, 1dp)
        public string TimeTaken = null; // the time taken (Xm, Ys)

        void Awake(){}

        /// <summary>
        /// Calculate various interesting statistics from the last level.
        /// </summary>
        public void Calculate(){
            // find the player
            GameObject[] playerObj = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject o in playerObj) {
                CylinderPlayer cylPl = o.GetComponent<CylinderPlayer>();
                if (cylPl != null) {
                    Player = cylPl;
                    break;
                }
            }
            if (Player == null) {
                throw new NullReferenceException("Player is still null after awake");
            }
            // calculate stats
            TeacupCount = UiIndicator.CurrentTeacupCount();
            ThreadUsed = this.CalculateThreadUsed();
            TimeTaken = this.CalculateTime();
        }

        /// <summary>
        /// Calculate the total thread used by the player and format it nicely as a string.
        /// </summary>
        /// <returns>A string with the metres of thread used to 2dp</returns>
        private string CalculateThreadUsed(){
            double threadUsedDec = Player.GetComponent<ThreadControl>().TotalThreadUsed();
            threadUsedDec = Math.Round(threadUsedDec, 1);
            return "" + threadUsedDec / 5;
        }

        /// <summary>
        /// Calculates the time the player took to finish the last level.
        /// </summary>
        /// <returns>A string with the time the player took to finish the level in
        /// Minutes:Seconds format</returns>
        private string CalculateTime(){
            float timeMillis = Player.timer.ElapsedMilliseconds;
            int timeSeconds = (int) timeMillis/1000;
            int timeMinutes = timeSeconds/60;
            timeSeconds -= timeMinutes*60;
            return timeMinutes + "m " + timeSeconds + "s";
        }
    }
}