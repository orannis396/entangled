﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Entangled{
    public class MusicControl : MonoBehaviour{
        private AudioSource cameraAudioSource;
        public AudioClip[] GameClips;
        public AudioClip[] MenuClips;
        public bool Active = true;
        public bool InGame = false;
        public bool Stopping = false;
        private int lastPlayed = -1;
        private float musicVol = 1f;

        public static MusicControl Instance;

        public static void PlayGameMusic(){
            if (Instance == null) return;
            Instance.Stopping = true;
            Instance.InGame = true;
        }

        public static void PlayMenuMusic(){
            if (Instance == null) return;
            Instance.InGame = false;
            Instance.Stopping = true;
        }

        public void Awake(){
            if (Instance != null) {
                this.Active = false;
                Destroy(this);
                return;
            }
            Instance = this;
            cameraAudioSource = this.GetComponent<AudioSource>();
            DontDestroyOnLoad(transform.gameObject);
            if (GameClips.Length == 0 || MenuClips.Length == 0) {
                this.Active = false;
            }
            musicVol = cameraAudioSource.volume;
        }

        public void FixedUpdate(){
            if (!Active) return;
            if (Stopping) {
                if (Math.Abs(cameraAudioSource.volume) < 0.01f) {
                    Stopping = false;
                    cameraAudioSource.Stop();
                    cameraAudioSource.volume = InGame ? musicVol*0.75f : musicVol;
                } else {
                    cameraAudioSource.volume -= 0.01f;
                }
            }
            if (cameraAudioSource.isPlaying) {
                return;
            }
            int nextSong = lastPlayed;
            int maxRange;
            switch (InGame) {
                case true:
                    maxRange = GameClips.Length;
                    break;
                default:
                    maxRange = MenuClips.Length;
                    break;
            }
            do {
                nextSong = Random.Range(0, maxRange);
            } while (nextSong == lastPlayed);
            switch (InGame) {
                case true:
                    cameraAudioSource.clip = GameClips[nextSong];
                    break;
                default:
                    cameraAudioSource.clip = MenuClips[nextSong];
                    break;
            }
            cameraAudioSource.Play();
        }
    }
}