﻿using UnityEngine;
using UnityEngine.Audio;
using Random = System.Random;

namespace Entangled{
    class AmbientMusicControl : MonoBehaviour{
        private readonly Random random = new Random();
        public AudioMixerGroup MixerGroup = null;
        public AudioClip[] Clips = null;
        public int NumberOfObjects = 3;
        public float PreferredDistance = 25.0f;
        private int soundI = 0;
        public void Awake(){
            GameObject[] ambientSoundObject = GameObject.FindGameObjectsWithTag("EnvironmentSound");
            if (ambientSoundObject.Length == 0) {
                Debug.LogWarning("No objects with EnvironmentSound tag");
                return;
            }
            GameObject lastObject = null;
            for (int i = 0; i <= NumberOfObjects; i++) {
                GameObject obj;
                do {
                    obj = ambientSoundObject[random.Next(0, ambientSoundObject.Length)];
                    if (lastObject == null) break;
                } while (Vector3.Distance(lastObject.transform.position, obj.transform.position) > PreferredDistance); 
                AudioSource s = obj.AddComponent<AudioSource>();
                this.ConfigureAudio(s);
                lastObject = obj;
            }
        }

        private void ConfigureAudio(AudioSource sc){
            sc.clip = Clips[soundI++];
            if (soundI == Clips.Length) {
                soundI = 0;
            }
            sc.volume = 1f;
            sc.outputAudioMixerGroup = MixerGroup;
            sc.spatialBlend = 1f;
            sc.loop = true;
            sc.PlayDelayed(random.Next(0,4));
            sc.maxDistance = 700;
            sc.pitch += (float)((random.NextDouble() - 0.35)*0.5); 
            sc.priority = 96;
        }
    }
}