﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Entangled.UI;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace Entangled{
    public class CylinderPlayer : MonoBehaviour{
        public bool InCutscene;

        public static readonly Dictionary<int, string> LevelDictionary = new Dictionary<int, string>{
            {1, "MazeOne"},
            {2, "MazeTwo"},
            {3, "MazeThree"},
            {4, "MazeFour"},
            {5, "MazeFive" }
        };

        public int CurrentLevel = 1;

        private const float CharacterHeight = 0.96f;
        private const float MinCamY = 0.3f;
        private const float MaxCamY = 4f;
        public const float PlayerRadius = 0.9f;
        public const float RotateAmount = 1.5f;
        public float ThreadPerStride = 2;
        public bool CanMove;
        public int TeacupScore = 0;

        public float RotOffset = 0f;

        public float Velocity = 0f;
        public float Acceleration;
        public float Deacceleration;

        public GameObject SpoolMesh;
        public GameObject SpoolBodyMesh;

        public float SpeedMultiplier;
        public const float WalkSpeed = 5f;
        public const float SpinConstant = 3f;
        public const int PickupRate = 2;

        public ThreadControl ThreadControl;
        private bool pickingUp = false;

        public Stopwatch timer = new Stopwatch();

    // Use this for initialization
    void Start(){
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.None;
        ThreadControl = this.GetComponent<ThreadControl>();
        timer.Start();
    }

        private void CheckForPickup(){
            // check if player has pressed pickup button
            if (Input.GetKey("p") || Input.GetAxis("PickupButton") != 0) {
                pickingUp = true;
                bool backwards = false;
                // rewind the thread
                for (int i = 0; i < PickupRate; i++) {
                    backwards = ThreadControl.Rewind(this);
                }
                // rotate player
                float rot = -(PickupRate*SpinConstant);
                if (backwards) rot = -rot;
                SpoolMesh.transform.Rotate(0f, 0f, rot);
                SpoolBodyMesh.transform.Rotate(0f, rot/2f, 0f);
            } else {
                pickingUp = false;
            }
        }

        void Awake(){
            EndOfLevelControl.IsCurrentlyActive = false;
        }

        void FixedUpdate(){
            if (InCutscene || !CanMove) return;
            this.CheckForPickup();
            this.Move();
        }

        private void Move(){
            this.CheckForDoor();
            float dx = Input.GetAxis("Horizontal");
            float dz = Input.GetAxis("Vertical");

            Vector2 conversionResults = ConvertInput(dx, dz);
            dx = conversionResults.x;
            dz = conversionResults.y;
            // if no input, return
            if (Math.Abs(dx) < 0.001 && Math.Abs(dz) < 0.001) {
                Velocity = 0f;
                return;
            }

            SpeedMultiplier = WalkSpeed;

            for (int i = 0; i < ThreadPerStride; i++) {
                // get input
                dz = conversionResults.y / ThreadPerStride;
                dx = conversionResults.x / ThreadPerStride;

                Velocity = Time.deltaTime*dz*SpeedMultiplier;
                RotOffset += dx*RotateAmount;

                // calculate old and new positions
                Vector3 oldPos = transform.position;
                Vector3 newPos = oldPos + (transform.forward*Time.deltaTime*dz*SpeedMultiplier);
                if (!pickingUp) {
                    if (ThreadControl.HasThread()) {
                        transform.position = newPos;
                        ThreadControl.UpdateThread(oldPos, transform.position, dx*RotateAmount, Velocity);
                    }
                    transform.Rotate(dx*RotateAmount*-1, 0f, 0f);
                    SpoolMesh.transform.Rotate(0f, 0f, PickupRate*SpinConstant*dz*0.8f);
                    SpoolBodyMesh.transform.Rotate(0f, PickupRate * SpinConstant * dz * 0.8f, 0f);
                }
            }
        }

        void OnTriggerEnter(Collider col){
            GameObject obj = col.gameObject;
            if (obj.CompareTag("Thread Pickup")) {
                ThreadPickup pickup = obj.GetComponent<ThreadPickup>();
                ThreadControl.PickUpThreadItem(pickup);
            }
        }

        Vector2 ConvertInput(float dx, float dz) {
            float horizMag = dx;
            float vertMag = dz;

            horizMag *= 3;
            vertMag *= 3;

            horizMag = Math.Min(Math.Max(horizMag, -1), 1);
            vertMag = Math.Min(Math.Max(vertMag, -1), 1);

            return new Vector2(horizMag, vertMag);
        }

        void CheckForDoor(){
            UnityEngine.Debug.DrawRay(transform.position, transform.forward*3, Color.cyan);
            RaycastHit objectHit;
            if (Physics.Raycast(transform.position, transform.forward, out objectHit, 3)) {
//raycast from player to camera
                GameObject obj = objectHit.collider.gameObject;
                if (obj.CompareTag("DoorRayTarget")) {
                    if (Input.GetKey(KeyCode.JoystickButton0) || Input.GetKey(KeyCode.Space))
                        obj.GetComponent<RaycastDoor>().Trigger();
                }
            }
        }
    }
}