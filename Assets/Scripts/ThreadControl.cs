﻿using System;
using UnityEngine;

namespace Entangled{
    /// <summary>
    ///     An interface class for controlling a chain of ThreadNodes.
    /// </summary>
    public class ThreadControl : MonoBehaviour {

        private ThreadPickup.ColorType color = ThreadPickup.ColorType.None; // the last ThreadPickup color
        private float currentAngle = 0f; // the angle the current thread is created at
        private float distanceSinceThread = 0f; // the distance covered since a thread node was created
        private float totalThreadUsed = 0f; // the total thread used in the whole level
        public ThreadNode EndThread = null; // the first thread of the 'max depth'
        public ThreadNode FrontThread = null; // the thread at the front of this chain
        public float FullThread = 100f; // the thread amount at which the thread body is at its max size
        public bool LimitedThread = true; // true if the player can only create a certain amount of thread
        
        private float prevAngle; // the angle the last thread was created at
        public float RemainingThread = 100f; // the amount of thread remaining
        public float StartThread = 100f; // thread to start with
        public float TbCenterSize = 10f; // the size of the center column of the spool (approx diameter size)
        private Vector3 tbFullScale; // the maximum size of the thread body
        public GameObject ThreadBase = null; // the kinematic thread attached to the player
        public GameObject ThreadBody = null; // the cylinder representing the thread on the player

        public GameObject ThreadPrefab = null; // the thread prefab the new threads are instatiated from
        private float MinDiam = 0.285f; // minimum diameter of thread body

        public GameObject ThreadParent; // empty parent of all thread

        private void Start(){
            RemainingThread = StartThread;
            FullThread = StartThread;
            tbFullScale = ThreadBody.transform.localScale;
            TbCenterSize = 0.2f;
            GiveColor(ThreadPickup.ColorType.None);
        }

        /// <summary>
        ///     Checks if a new thread needs to be created using the distance the player has
        ///     travelled this frame.
        /// </summary>
        /// <param name="a">The player's starting position</param>
        /// <param name="b">The player's ending position</param>
        /// <param name="angle">The angle travelled by the player (in degrees)</param>
        public void UpdateThread(Vector3 a, Vector3 b, float angle, float velocity){
            currentAngle = angle;
            float distance = Vector3.Distance(a, b);
            angle = Mathf.Abs(angle);
            this.UpdateDistance(distance);
            bool movingBackwards = velocity < 0;
            this.AddThread(movingBackwards);
            prevAngle = angle;
            this.UpdateThreadBody();
        }

        /// <summary>
        ///     Updates the distance travelled by the player since the last thread node
        ///     was created.
        /// </summary>
        /// <param name="distance">The distance travelled in this frame</param>
        private void UpdateDistance(float distance){
            // add distances to distance since thread
            distanceSinceThread += distance;
        }

        /// <summary>
        ///     Adds a thread to the front of the chain.
        /// </summary>
        private ThreadNode AddThread(bool movingBackwards){
            if (distanceSinceThread >= ThreadNode.Length) {
                ThreadNode newNode = this.AddNewToFront(movingBackwards);
                distanceSinceThread -= ThreadNode.Length;
                RemainingThread -= ThreadNode.Length;
                totalThreadUsed += ThreadNode.Length;
                return newNode;
            }
            return null;
        }

        /// <summary>
        ///     Adds a new thread node to the front of the thread chain.
        ///     This includes shifting down all other thread nodes in the linked list.
        ///     The front thread is always kinematic.
        /// </summary>
        /// <returns>The thread node's gameobject</returns>
        public ThreadNode AddNewToFront(bool movingBackwards){
            // calculate position
            Vector3 baseThreadPos = ThreadBase.transform.position;
            Vector3 newPos = new Vector3(baseThreadPos.x, baseThreadPos.y, baseThreadPos.z);

            // instantiate thread
            GameObject newThread = (GameObject) Instantiate(ThreadPrefab, newPos, Quaternion.identity);
            if(ThreadParent != null) newThread.transform.parent = ThreadParent.transform;
            DeferredDestroy.DeferDestroy(newThread);
            ThreadNode newNode = newThread.GetComponent<ThreadNode>();

            // adjust list
            if (FrontThread != null) {
                newNode.SetChild(FrontThread);
                FrontThread.SetParent(newNode);
                FrontThread.MoveDown();
            } else {
                EndThread = newNode;
            }

            this.SetFront(newNode);

            // adjust rotation
            Quaternion baseRot = ThreadBase.transform.rotation;
            if (newNode.GetChild() != null && FrontThread != null && !this.MovingStraight()) {
                Quaternion prevRot = newNode.GetChild().transform.rotation;
                float x = (baseRot.x + prevRot.x)/2f;
                float y = (baseRot.y + prevRot.y)/2f;
                float z = (baseRot.z + prevRot.z)/2f;
                float w = (baseRot.w + prevRot.w)/2f;
                newThread.transform.rotation = new Quaternion(x, y, z, w);
            } else {
                newThread.transform.rotation = new Quaternion(baseRot.x, baseRot.y, baseRot.z, baseRot.w);
            }

            // tell node if it's facing backwards
            if (movingBackwards) {
                newNode.Backwards = true;
            }

            // change color
            ChangeColor(newThread);

            return newNode;
        }

        /// <summary>
        ///     Updates the size of the thread body based on the remaining thread.
        /// </summary>
        public void UpdateThreadBody(){
            if (FullThread == 0f || !LimitedThread) {
                return;
            }
            float percentThreadLeft = RemainingThread/FullThread;
            float newDiam = MinDiam + (tbFullScale.x - TbCenterSize)*percentThreadLeft;

            if (newDiam < 0 || tbFullScale.x < newDiam) {
                return;
            }

            ThreadBody.transform.localScale = new Vector3(newDiam, tbFullScale.y, newDiam);
        }

        /// <summary>
        /// Sets the front thread node in the chain.
        /// </summary>
        /// <param name="front">New front node</param>
        public void SetFront(ThreadNode front){
            FrontThread = front;
        }

        /// <summary>
        ///     Determines whether there is any remaining thread.
        /// </summary>
        /// <returns>true iff thread remains</returns>
        public bool HasThread(){
            if (LimitedThread) {
                return RemainingThread >= ThreadNode.Length;
            }
            return true;
        }

        /// <summary>
        /// Searches for the thread node at the front of the chain.
        /// This is sometimes necessary as some Unity behaviour results in the front
        /// thread being set to null.
        /// </summary>
        /// <returns>True iff the front thread was successfully found.</returns>
        private bool FindFront(){
            if (EndThread == null) {
                return false;
            }
            ThreadNode current = EndThread;
            while (current.Parent != null) {
                current = current.Parent;
            }
            FrontThread = current;
            ThreadNode.SetDepthRec(FrontThread, 0, this);
            return true;
        }

        /// <summary>
        ///     Calculates whether the thread is currently being created in a straight line.
        /// </summary>
        /// <returns>true iff the thread is moving in a straight line</returns>
        public bool MovingStraight(){
            return Mathf.Abs(currentAngle - prevAngle) < 0.1f;
        }

        /// <summary>
        ///     This method should be called when a Thread Pickup item has been collected.
        ///     An amount of thread will be added to the player, and their thread will
        ///     change colour.
        /// </summary>
        /// <param name="pickup">The Thread Pickup item that has been picked up</param>
        public void PickUpThreadItem(ThreadPickup pickup){
            if (pickup.HasThread()) {
                this.AddThreadLength(pickup.Amount);
                pickup.TakeThread();
            }
            color = pickup.Color;
            ThreadPickup.ChangeObjectColor(ThreadBody, color);
        }

        /// <summary>
        /// Changes the colour of a GameObject.
        /// </summary>
        /// <param name="obj">The object to change the colour of.</param>
        private void ChangeColor(GameObject obj){
            ThreadPickup.ChangeObjectColor(obj, color);
        }

        /// <summary>
        ///     Adds to the length of thread remaining.
        /// </summary>
        /// <param name="add">The length of thread to add.</param>
        public void AddThreadLength(float add){
            RemainingThread += add;
        }

        /// <summary>
        /// Moves the player backwards along the thread and changing its angle appropriately.
        /// Picks up the thread as it goes.
        /// </summary>
        /// <param name="player"></param>
        public bool Rewind(CylinderPlayer player){
            // find front thread
            if (FrontThread == null) {
                if (!this.FindFront()) {
                    return false;
                }
            }
            // determine new position
            Vector3 frontPos = FrontThread.transform.position;
            frontPos.y = CylinderPlayer.PlayerRadius;
            Quaternion frontRot = FrontThread.transform.rotation;
            bool backwards = FrontThread.Backwards;
            // remove front thread
            this.RemoveFront();
            // set new positions
            GameObject obj = player.gameObject;
            obj.transform.position = frontPos;
            obj.transform.rotation = frontRot;
            obj.transform.Rotate(new Vector3(90f, 90f, 0f), Space.Self);
            return backwards;
        }

        private bool RemoveFront(){
            // attempt to find the front thread - if unsuccessful, return
            if (FrontThread == null) {
                if (!FindFront()) {
                    return false;
                }
            }
            // we know now that FrontThread is not null - remove it
            FrontThread.gameObject.SetActive(false);
            // update child to be at the front
            ThreadNode child = FrontThread.Child;
            if (child != null) {
                ThreadNode.SetDepthRec(child, 0, this);
                this.SetFront(child);
                child.SetParent(null);
            } else {
                this.SetFront(null);
            }
            // update remaining thread
            RemainingThread += ThreadNode.Length;
            totalThreadUsed -= ThreadNode.Length;
            this.UpdateThreadBody();
            return true;
        }

        /// <summary>
        /// Set the colour of the thread.
        /// </summary>
        /// <param name="c">The colour to change the thread to.</param>
        public void GiveColor(ThreadPickup.ColorType c){
            color = c;
            ThreadPickup.ChangeObjectColor(ThreadBody, color);
        }

        /// <summary>
        /// Gets the current colour of the thread.
        /// </summary>
        /// <returns>The current thread colour</returns>
        public ThreadPickup.ColorType GetColor(){
            return color;
        }

        /// <summary>
        /// Sets the colour of the thread back to the default.
        /// </summary>
        public void RemoveColor(){
            color = ThreadPickup.ColorType.None;
            ThreadPickup.ChangeObjectColor(ThreadBody, color);
        }

        /// <summary>
        /// Determines whether the thread is currently the given colour.
        /// </summary>
        /// <param name="c">The colour to check for</param>
        /// <returns>True iff the thread is currently colour c</returns>
        public bool HasColor(ThreadPickup.ColorType c){
            return color == c;
        }

        /// <summary>
        /// Returns the total thread in metres used in this level.
        /// </summary>
        /// <returns>Total thread in metres used in this level.</returns>
        public float TotalThreadUsed() {
            return totalThreadUsed;
        }
    }
}