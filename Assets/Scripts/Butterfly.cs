﻿using UnityEngine;
using System.Collections;

public class Butterfly : MonoBehaviour {

    public GameObject ButterflyBounds = null;



    private Vector3 targetPos;
    private Vector3 boxSize;
    private float w, h, d;
    private float framesSpentFlying = 0f;
    private float randFlyingTime = 0f;


	// Use this for initialization
	void Start () {
        boxSize = ButterflyBounds.GetComponent<Renderer>().bounds.size;
        w = boxSize.x;
        h = boxSize.y;
        d = boxSize.z;

        ChangeDirection();
        randFlyingTime = Random.Range(50, 500);
        //Debug.Log(ButterflyBounds + " /// " + w + " /// " + h + " /// " + d);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        if (framesSpentFlying >= randFlyingTime) {
            ChangeDirection();
            framesSpentFlying = 0f;
        } else {
            framesSpentFlying++;
            transform.LookAt(targetPos);
            transform.localPosition += transform.forward * 0.003f;
        }

        EnsureWithinBounds();

	}

    void EnsureWithinBounds() {
        if(transform.localPosition.x > w || transform.localPosition.y > h || transform.localPosition.z > d) {
            if(transform.localPosition.x < 0 || transform.localPosition.y < 0 || transform.localPosition.z < 0) {
                ChangeDirection();
            }
        }
    }

    void ChangeDirection() {
        targetPos = new Vector3(ButterflyBounds.GetComponent<Renderer>().bounds.center.x + Random.Range(-w/2, w / 2),
            ButterflyBounds.GetComponent<Renderer>().bounds.center.y + Random.Range(-h / 2, h / 2),
            ButterflyBounds.GetComponent<Renderer>().bounds.center.z + Random.Range(-d / 2, d / 2));
        if (!ButterflyBounds.GetComponent<Renderer>().bounds.Contains(targetPos)) {
            ChangeDirection();
        }
    }
}
