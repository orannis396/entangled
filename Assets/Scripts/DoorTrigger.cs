﻿using UnityEngine;

namespace Entangled{
    public class DoorTrigger : MonoBehaviour{
        public GameObject Door;
        private bool triggered = false; // true if the player is in the trigger
        private bool hasColor = false; // true if the player has the right color to open this door
        private bool hasMoved = false; // true if the door has started moving
        public ThreadPickup.ColorType Color; // this door's color
        public CylinderPlayer Player; // the player
        private ThreadControl tc;

        void Start(){
            tc = Player.GetComponent<ThreadControl>();
        }

        // Update is called once per frame
        void FixedUpdate(){
            // check if the player has the color to open this door
            if (Color != ThreadPickup.ColorType.None && !hasColor) {
                hasColor = tc.HasColor(Color);
            }
            // if door has been triggered, and is allowed to open move it
            if ((triggered || hasMoved) && (hasColor || Color == ThreadPickup.ColorType.None)) {
                hasMoved = true;
                tc.RemoveColor();
                Door.transform.position -= new Vector3(0f, 0.08f, 0f);
            }
            // if door has finished opening, destroy it
            if (Door.transform.position.y < -6f) {
                Destroy(Door);
            }
        }

        void OnTriggerEnter(Collider c){
            if (c.gameObject.CompareTag("Player")) {
                triggered = true;
            }
        }

        void OnTriggerExit(Collider c){
            if (c.gameObject.CompareTag("Player")) {
                triggered = false;
            }
        }
    }
}