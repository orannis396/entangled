﻿using UnityEngine;

namespace Entangled{
    /// <summary>
    /// A class to represent a small part of a whole chain-like thread.
    /// ThreadNodes are connected in the style of a two-way linked list.
    /// </summary>
    public class ThreadNode : MonoBehaviour{
        public const float Length = 0.05f; // the length of one thread node
        public const int MaxDepth = 1; // the depth at which calculations on nodes should stop
        public ThreadNode Parent; // the parent of this node
        public ThreadNode Child; // the child of this node
        public int Depth = 0; // could instead just use count when searching
        public bool Backwards = false;

        /// <summary>
        /// Recursively increases depth of all thread nodes, up to MaxDepth
        /// </summary>
        public void MoveDown(){
            Depth++;
            if (Depth == MaxDepth) {
                return;
            }
            if (Child != null) {
                Child.MoveDown();
            }
        }

        /// <summary>
        /// Remove this thread from the chain.
        /// </summary>
        /// <param name="tc">The thread control in charge of this thread</param>
        public void Remove(ThreadControl tc){
            //Debug.Log(depth);
            if (Depth == 0) {
                // update child to put it in front
                if (Child != null) {
                    SetDepthRec(Child, 0, tc);
                    tc.SetFront(Child);
                    Child.SetParent(null);
                } else {
                    tc.SetFront(null);
                }
                // update thread control
                tc.RemainingThread += Length;
                tc.UpdateThreadBody();
                
                // remove this object
                gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Recursively sets the depth of all nodes down the thread chain from a starting node and currentDepth.
        /// </summary>
        /// <param name="node">The node to start with</param>
        /// <param name="currentDepth">The starting depth</param>
        /// <param name="tc">The ThreadControl instance that holds this thread node</param>
        public static void SetDepthRec(ThreadNode node, int currentDepth, ThreadControl tc){
            // base cases
            if (node == null) {
                return;
            } else if (currentDepth >= MaxDepth) {
                tc.EndThread = node;
                //node.GetComponent<Collider>().enabled = false;
                return;
            }
            // update current node
            node.SetDepth(currentDepth);
            // recurse down
            SetDepthRec(node.GetChild(), currentDepth + 1, tc);
        }

        #region getandset

        public ThreadNode GetParent(){
            return Parent;
        }

        public ThreadNode GetChild(){
            return Child;
        }

        public void SetParent(ThreadNode newParent){
            Parent = newParent;
        }

        public void SetChild(ThreadNode newChild){
            Child = newChild;
        }

        public int GetDepth(){
            return Depth;
        }

        public void IncDepth(){
            Depth++;
        }

        public void DecDepth(){
            Depth--;
        }

        public void SetDepth(int d){
            Depth = d;
        }

        #endregion
    }
}