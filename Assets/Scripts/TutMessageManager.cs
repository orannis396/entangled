﻿using UnityEngine;
using System.Collections;
using Entangled.UI;

public class TutMessageManager : MonoBehaviour {

    public GameObject ControllerTutMessages = null;
    public GameObject MouseKeyboardTutMessages = null;

	// Use this for initialization
	void Start () {
        if (MenuScript.ControllerConnected) {
            ControllerTutMessages.SetActive(true);
            MouseKeyboardTutMessages.SetActive(false);
        } else {
            ControllerTutMessages.SetActive(false);
            MouseKeyboardTutMessages.SetActive(true);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}

}
