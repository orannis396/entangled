﻿using UnityEngine;

namespace Entangled{
    /// <summary>
    /// Controls the movement of certain pickup items
    /// </summary>
    public class PickupMovement : MonoBehaviour{
        private Dir direction = Dir.Up; // the direction the pickyp is currently moving in
        public int MaxHeight = 500; // the maximum ticks the pickup may reach

        public GameObject Obj; // the gameobject to animate
        private int ticksSinceLastSwitch; // ticks since the object changed direction

        private void Awake(){}

        private void FixedUpdate(){
            Obj.transform.Rotate(0f, 0.5f, 0f);

            ticksSinceLastSwitch++;

            if (direction == Dir.Up) {
                // update position
                Obj.transform.position += new Vector3(0f, 0.002f, 0f);
                // check for direction change
                if (ticksSinceLastSwitch > MaxHeight) {
                    direction = Dir.Down;
                    ticksSinceLastSwitch = 0;
                }
            } else {
                // update position
                Obj.transform.position -= new Vector3(0f, 0.002f, 0f);
                // check for direction change
                if (ticksSinceLastSwitch > MaxHeight) {
                    direction = Dir.Up;
                    ticksSinceLastSwitch = 0;
                }
            }
        }

        /// <summary>
        /// Simple direction enum
        /// </summary>
        private enum Dir{
            Up,
            Down
        }
    }
}