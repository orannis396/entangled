﻿using UnityEngine;
using System.Collections;

namespace Entangled{
    public class RaycastDoor : MonoBehaviour{
        public GameObject DoorLeft = null;
        public GameObject DoorRight = null;
        public GameObject SpoolEmpty = null;

        public GameObject Collider = null;

        public bool Triggered = false;
        public bool FinishedAnim = false;
        public AudioSource DoorSound = null;
        private Animation leftAnim = null;
        private Animation rightAnim = null;
        private Animation spoolAnim = null;

        private bool hasColor = false; // true if the player has the right color to open this door
        public ThreadPickup.ColorType Color; // this door's color
        private ThreadControl threadCtrl = null;

        void Start(){
            leftAnim = DoorLeft.GetComponent<Animation>();
            rightAnim = DoorRight.GetComponent<Animation>();
            spoolAnim = SpoolEmpty.GetComponent<Animation>();

            /*leftAnim.wrapMode = WrapMode.Once;
        rightAnim.wrapMode = WrapMode.Once;
        spoolAnim.wrapMode = WrapMode.Once;*/
        }

        void Awake(){
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players) {
                ThreadControl tc = player.GetComponent<ThreadControl>();
                if (tc != null) {
                    threadCtrl = tc;
                    break;
                }
            }
        }

        void FixedUpdate(){
            if (Triggered && !FinishedAnim) {
                leftAnim.Play("LeftDoorAnimation");
                rightAnim.Play("RightDoorAnimation");
                spoolAnim.Play("SpoolDoorAnimation");
                if (DoorSound != null) {
//                    DoorSound.Play();
                } else {
                    Debug.LogError("Door sound was null");
                }
                FinishedAnim = true;
            }
            if (Triggered && !spoolAnim.isPlaying) {
                SpoolEmpty.SetActive(false);
                Collider.SetActive(false);
            }
        }

        public void Trigger(){
            // check if the player has the color to open this door
            if (Triggered) {
                return;
            }
            hasColor = threadCtrl.HasColor(Color);
            Triggered = hasColor || Color == ThreadPickup.ColorType.None;
            if (Triggered) {
                threadCtrl.RemoveColor();
            }
        }
    }
}