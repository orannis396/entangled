﻿using Entangled.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Entangled{
    class MenuReturn : MonoBehaviour{
        public void Update(){
            /*if (Input.GetKeyUp(KeyCode.Backslash)) {
                SceneManager.LoadScene("MainMenu");
                if (!MenuScript.ControllerConnected) {
                    Cursor.visible = true;
                    Cursor.lockState = CursorLockMode.None;
                }
                DeferredDestroy.TriggerDestroy();
                MusicControl.PlayMenuMusic();
            }*/
            if (Input.GetKeyUp(KeyCode.RightBracket)) {
                SceneManager.LoadScene("EndOfLevel",LoadSceneMode.Additive);
            }
        }
    }
}