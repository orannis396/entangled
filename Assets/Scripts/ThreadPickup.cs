﻿using System.Collections.Generic;
using UnityEngine;

namespace Entangled{
    /// <summary>
    /// Represents extra thread the player can pick up.
    /// May be a certain colour, and will change the player's colour when picked up.
    /// </summary>
    public class ThreadPickup : MonoBehaviour{
        /// <summary>
        /// A simple enum listing the available thread colours.
        /// </summary>
        public enum ColorType{
            None,
            Red,
            Blue,
            Pink,
            Yellow
        };

        /// <summary>
        /// Maps the ColorTypes to actual colours.
        /// </summary>
        private static readonly Dictionary<ColorType, Color> ColorKey = new Dictionary<ColorType, Color>(){
            {ColorType.None, UnityEngine.Color.gray},
            {ColorType.Red, UnityEngine.Color.red},
            {ColorType.Blue, UnityEngine.Color.blue},
            {ColorType.Pink, UnityEngine.Color.magenta},
            {ColorType.Yellow, UnityEngine.Color.yellow}
        };

        public float Amount; // amount of thread contained in this pickup
        public ColorType Color = ColorType.None; // the colour of the thread
        private bool hasThread = true; // if the pickup will give the player extra thread

        void Start(){
            ChangeObjectColor(gameObject, Color);
            if (this.GetComponentInChildren<ParticleSystem>() != null) {
                ChangeParticleColor(gameObject, Color);
            }
        }

        /// <summary>
        /// Changes the colour of a gameobject, presumably some kind of thread object.
        /// </summary>
        /// <param name="obj">The object whose colour we will change.</param>
        /// <param name="color">The colout to change to.</param>
        public static void ChangeObjectColor(GameObject obj, ColorType color){
            obj.GetComponent<Renderer>().material.color = ColorKey[color];
        }

        /// <summary>
        /// Changes the particle colour of a gamobject.
        /// Assumes the gameobject (or its children) has a particle system.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="color"></param>
        public static void ChangeParticleColor(GameObject obj, ColorType color){
            var col = ColorKey[color];
            col.a = 0.1f;
            obj.GetComponentInChildren<ParticleSystem>(true).startColor = col;
        }

        /// <summary>
        /// Determines whether this thread pickup has thread remaining.
        /// </summary>
        /// <returns>True iff there is thread remaining.</returns>
        public bool HasThread(){
            return hasThread;
        }

        /// <summary>
        /// Removes the thread from this pickup, hides the thread.
        /// </summary>
        public void TakeThread(){
            hasThread = false;
            this.GetComponent<Renderer>().enabled = false;
        }
    }
}