﻿using System;
using Entangled.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Entangled{
    public class ExitLevel : MonoBehaviour{
        public string NextLevel = null;
        public GameObject Player = null;

        private bool StartedSceneFinish = false;

        public Camera PlayerCamera = null;
        public Camera EndSceneCamera = null;

        public Image PlayerCamOverlay = null;
        public Image EndSceneCamOverlay = null;
        public float FadeSpeed;

        public AudioListener PlayerCamAudioListener = null;
        public AudioListener EndSceneCamAudioListener = null;

        private float overlayOpacity = 0f;
        private bool loadedEndScene = false;

        void Start() {
            PlayerCamAudioListener = PlayerCamera.GetComponent<AudioListener>();
            EndSceneCamAudioListener = EndSceneCamera.GetComponent<AudioListener>();

            EndSceneCamAudioListener.enabled = false;
        }

        // Update is called once per frame
        void Update(){
            if (!StartedSceneFinish) return;
            Color col = PlayerCamOverlay.color;
            if (col.a >= 3f) {
                
            } else {
                overlayOpacity += FadeSpeed;
                col.a = overlayOpacity;
                PlayerCamOverlay.color = col;
                //OverlayImage.color.a++
            }
        }

        void OnTriggerEnter(Collider c){
            if (!c.gameObject.CompareTag("Player")) return;
            try {
                CylinderPlayer cp = Player.GetComponent<CylinderPlayer>();
                cp.CanMove = false;
                cp.timer.Stop();
            }
            catch (Exception e){
                Debug.LogError(e);
            }
            //            StartedSceneFinish = true;
        }

        void OnTriggerStay(Collider c) {
            if (!c.gameObject.CompareTag("Player")) return;
            Color playerCamCol = PlayerCamOverlay.color;
            Color endSceneCamCol = EndSceneCamOverlay.color;

            if (!loadedEndScene) {
                if (PlayerCamera.enabled == true) {
                    if (playerCamCol.a < 1f) {//player cam hasnt finished fading out yet
                        playerCamCol.a += FadeSpeed;
                        PlayerCamOverlay.color = playerCamCol;
                    } else {
                        SceneManager.LoadScene("EndOfLevel", LoadSceneMode.Additive);
                        loadedEndScene = true;
                    }
                }
            } else {
                if (endSceneCamCol.a > 0f) {
                    endSceneCamCol.a -= FadeSpeed;
                    EndSceneCamOverlay.color = endSceneCamCol;
                }
                EndSceneCamera.gameObject.SetActive(true);
                EndSceneCamera.enabled = true;
                PlayerCamera.enabled = false;
                PlayerCamera.gameObject.SetActive(false);
                EndSceneCamAudioListener.enabled = true;
                PlayerCamAudioListener.enabled = false;
            }
        }
    }
}