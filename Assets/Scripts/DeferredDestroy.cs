﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Entangled{
    public class DeferredDestroy : MonoBehaviour{
        private static DeferredDestroy Instance{
            get { return instance; }
            set { instance = value; }
        }

        private readonly Queue<GameObject> objects = new Queue<GameObject>();
        private readonly Queue<GameObject> destroying = new Queue<GameObject>();
        private bool active = false;
        private bool currentlyDestroying = false;

        private string currentLevel = null;
        private static DeferredDestroy instance = null;

        public void Awake(){
            if (Instance != null) {
                Destroy(gameObject, 0.2f);
                return;
            }
            Instance = this;
            active = true;
            currentLevel = SceneManager.GetActiveScene().name;
            DontDestroyOnLoad(gameObject);
        }

        public void Update(){
            if (active) {
                if (currentLevel != SceneManager.GetActiveScene().name) {
                    currentLevel = SceneManager.GetActiveScene().name;
                    TriggerDestroy();
                }
                if (!currentlyDestroying) return;
                for (int i = 0; i < ((1.0f/Time.smoothDeltaTime)/2) + 1; i++) {
                    if (destroying.Count == 0) {
                        currentlyDestroying = false;
                        break;
                    }
                    Destroy(destroying.Dequeue());
                }
                if (destroying.Count == 0) {
                    currentlyDestroying = false;
                }
            }
        }

        void OnLevelWasLoaded(int level){
            TriggerDestroy();
        }

        public static void DeferDestroy(GameObject o){
            if (Instance != null) {
                Instance.DeferDestroyq(o);
            }
        }

        private void DeferDestroyq(GameObject o){
            if (active) {
                if (o == null) {
                    Debug.LogWarning("Defer object was null, ignoring");
                    return;
                }
                DontDestroyOnLoad(o.transform.root);
                objects.Enqueue(o);
            }
        }

        public static void TriggerDestroy(){
            if (Instance != null) {
                Instance.TriggerThisDestroy();
            }
        }

        private void TriggerThisDestroy(){
            if (active) {
                foreach (GameObject o in objects) {
                    o.GetComponentInChildren<Renderer>().enabled = false;
                    destroying.Enqueue(o);
                }
                objects.Clear();
                currentlyDestroying = true;
            }
        }
    }
}